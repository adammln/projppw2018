
function changeTheme() {

	var pg1 = document.getElementById("page1")
	pg1.style.background == "gold"
	var pg2 = document.getElementById("page2")
	var pg3 = document.getElementById("page3")
	if (pg1.style.background == "gold") {	
		pg1.style.background = "darkgrey"
		pg2.style.background = "grey"
		pg3.style.background = "azure"
		pg3.style.color = "black"

	} else {
		pg1.style.background = "gold"
		pg2.style.background = "lightblue"
		pg3.style.background = "navy"
		pg3.style.color = "white"

	}
	

}

(function($) {
    
    var allPanels = $('.accordion > dd').hide();

    $('.accordion > dt > button').click(function() {
      allPanels.slideUp();
      $(this).parent().next().slideDown();
      return false;
    });
})(jQuery);

$(document).ready(function() {
 

    var url='https://mulonist.herokuapp.com/booksapi/'

    $('#button2').click(function() {
        var btn = document.getElementById("button2");
        var table = document.getElementById("book-collections");
        table.style.visibility = "visible";
        btn.style.visibility = "hidden";
        $.ajax("/booksapi")
        .done(function(data) {
            data = data['books'];
            for (var i = 0; i < data.length; i++ ) {
                var row = '<tr>';
                
                row +=  '<th scope="row">'+ (i+1) +'</th>'                
                row += '<td scope="col">' + '<img src="'+ data[i].smallThumbnail +'" class="rounded mx-auto d-block" alt="'+data[i].previewLink+'">' + '</th>';
                row += '<td scope="col">' + data[i].title + '</td>';                  
                row += '<td scope="col">' + data[i].authors + '</td>';
                row += '<td scope="col">' + data[i].publishedDate + '</td>';
                row += '<td> <span class="fav-star">☆</span></td>';
                row += '</tr>';                                   
                $('#volumes-table-body').append(row);
            }
        });
    });

    $('#searchBTN').click(function() {
        document.getElementById("volumes-table-body").empty();
        $.ajax("/searchBooks")
        .done(function(data) {
            searchData = data['books'];
            for (var i = 0; i < data.length; i++ ) {
                var row = '<tr>';
                
                row +=  '<th scope="row">'+ (i+1) +'</th>'                
                row += '<td scope="col">' + '<img src="'+ searchData[i].smallThumbnail +'" class="rounded mx-auto d-block" alt="'+searchData[i].previewLink+'">' + '</th>';
                row += '<td scope="col">' + searchData[i].title + '</td>';                  
                row += '<td scope="col">' + searchData[i].authors + '</td>';
                row += '<td scope="col">' + searchData[i].publishedDate + '</td>';
                row += '<td> <span class="fav-star">☆</span></td>';
                row += '</tr>';                                   
                $('#volumes-table-body').append(row);
            }
        });
    });

    var countFav = 0;
    udpateFav();
    function udpateFav() {
        $('#favCount').text(countFav);
    }

    $(document).on('click', '.fav-star', function() {
        if ($(this).text() == "☆") {
            countFav++;
            udpateFav();
            $(this).text("★");
        } else {
            countFav--;
            udpateFav();
            $(this).text("☆");
        }
    });

});

function onSignIn(googleUser) {
  var profile = googleUser.getBasicProfile();
  console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
  console.log('Name: ' + profile.getName());
  console.log('Image URL: ' + profile.getImageUrl());
  console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
}