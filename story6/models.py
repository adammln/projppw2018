from django.db import models
from django.utils import timezone

class UpdateStatus(models.Model):
	statusContent = models.TextField(max_length=256)
	dateTime = models.DateTimeField(default=timezone.now)

class AccountRegist(models.Model):
	username = models.CharField(max_length=64)
	#email = models.EmailField()
	password = models.CharField(max_length=64)


# Create your models here.
