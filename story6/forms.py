from django import forms

class StatusForm(forms.Form):
	statusContent = forms.CharField(label='',
		required=True,
		max_length=256,
		widget=forms.TextInput(attrs={'class' : 'input-group-text',
			'id' : 'status_field',
			'type':'text',
			'placeholder':'What\'s up?'})
		)

class RegistrationForm(forms.Form):
	username = forms.CharField(
		label='',
		required=True,
		max_length=64,
		widget=forms.TextInput(attrs={
			'class' : 'input-group-text',
			'name' : 'username',
			'id' : 'username1',
			'type' : 'text',
			'placeholder' : 'username',
			})
		)
	#email = forms.EmailField(
	#	label='',
	#	required=True,
	#	max_length=64,
	#	widget=forms.EmailInput(attrs={
	#		'class' : 'input-group-text',
	#		'name' : 'email_field',
	#		'id' : 'email_field1',
	#		'placeholder' : 'e-mail',
	#		})
	#	)
	password = forms.CharField(
		label='',
		required=True,
		max_length=64,
		min_length=8,
		widget=forms.PasswordInput(attrs={
			'class' : 'input-group-text',
			'name' : 'password_field',
			'id' : 'password_field1',
			'placeholder' : 'password',
			})
		)