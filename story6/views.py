from django.contrib.auth import authenticate, login, logout
from rest_framework import serializers
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

#from request.exceptions import HTTPError

from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.shortcuts import render


from .models import *
from .forms import *

import json, requests

response = {}
session = {}

def load_landing_page(request):
	response['new_status_field'] = StatusForm()
	response['status_all'] = UpdateStatus.objects.all()
	return render(request, 'header.html', response)

def status_save(request):
	form = UpdateStatus(request.POST or None)                       # UNTESTED
	if (request.method == 'POST'):
		response['statusContent'] = request.POST['statusContent']  
		new_status = UpdateStatus(
			statusContent=response['statusContent'])
		new_status.save()
		return HttpResponseRedirect('/main/')
	else : 
		form = UpdateStatus()
	return render(request, 'header.html')							# -----till here

def get_books_api(request):
	books = list()
	response = requests.get(url='https://www.googleapis.com/books/v1/volumes?q=quilting')
	data = response.json()
	for i in range (10) : 
		books.append({
			'id' : data['items'][i]['id'],
			'smallThumbnail' : data['items'][i]['volumeInfo']['imageLinks']['smallThumbnail'],
			'title' : data['items'][i]['volumeInfo']['title'],
			'authors' : data['items'][i]['volumeInfo']['authors'],
			'publishedDate' : data['items'][i]['volumeInfo']['publishedDate'],
			'previewLink' : data['items'][i]['volumeInfo']['previewLink'],
			})
	return JsonResponse({'books' : books})

def search_book(request, url):
	apiURL = 'https://www.googleapis.com/books/v1/volumes?q='
	if (request.method == 'POST') : # this will be GET now      
		book_name =  request.POST.get('search-box')
		print (book_name)
		apiURL += book_name.lower()    
	

# 	books = list()
# 	response = requests.get(url=apiURL)
# 	searchData = response.json()
# 	for i in range (10) : 
# 		books.append({
# 			'id' : searchData['items'][i]['id'],
# 			'smallThumbnail' : searchData['items'][i]['volumeInfo']['imageLinks']['smallThumbnail'],
# 			'title' : searchData['items'][i]['volumeInfo']['title'],
# 			'authors' : searchData['items'][i]['volumeInfo']['author'],
# 			'publishedDate' : searchData['items'][i]['volumeInfo']['publishedDate'],
# 			'previewLink' : searchData['items'][i]['volumeInfo']['previewLink'],
# 			})
# 	return JsonResponse({'books' : books})

# def register_account(request):
# 	response['registration_form'] = RegistrationForm()
# 	return render(request, 'headerRegist.html', response)

# def regist_validate_save(request):
# 	return render(request, 'header.html')

# def user_info_to_json(request):
# 	name = request.GET['name']
# 	email = request.GET['email']
# 	password = request.GET['password']
# 	return JsonResponse( {"name" : name,"email" : email, "password" : password}, safe=False )

def my_view(request):
	if request.method == 'POST':
		username = request.POST['username']
		password = request.POST['password']
		response['user'] = authenticate(request, username=username, password=password)
		if response['user'] is not None :
			response['user'].save()
			return HttpResponseRedirect('/main/')
		else : 
			return HttpResponse(status=403)
	else :	
		response['registration_form'] = RegistrationForm()
		return render(request, 'headerRegist.html', response)
	# Create a state token to prevent request forgery.
  

def logout_view(request):
	logout(request)
	for key in request.session.keys():
		del request.session[key]
	return HttpResponseRedirect('/login')


#referrence : https://github.com/coriolinus/oauth2-article/blob/master/views.py
