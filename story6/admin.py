from django.contrib import admin

from .models import UpdateStatus, AccountRegist

admin.site.register(UpdateStatus)
admin.site.register(AccountRegist)


# Register your models here.
