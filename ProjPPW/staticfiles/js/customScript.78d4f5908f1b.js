
function changeTheme() {

	var pg1 = document.getElementById("page1")
	pg1.style.background == "gold"
	var pg2 = document.getElementById("page2")
	var pg3 = document.getElementById("page3")
	if (pg1.style.background == "gold") {	
		pg1.style.background = "darkgrey"
		pg2.style.background = "grey"
		pg3.style.background = "azure"
		pg3.style.color = "black"

	} else {
		pg1.style.background = "gold"
		pg2.style.background = "lightblue"
		pg3.style.background = "navy"
		pg3.style.color = "white"

	}
	

}

(function($) {
    
    var allPanels = $('.accordion > dd').hide();

    $('.accordion > dt > button').click(function() {
      allPanels.slideUp();
      $(this).parent().next().slideDown();
      return false;
    });
})(jQuery);

$(document).ready(function() {
 

    var url='https://mulonist.herokuapp.com/booksapi/'

    $('#button2').click(function() {
        var btn = document.getElementById("button2");
        var table = document.getElementById("book-collections");
        table.style.visibility = "visible";
        btn.style.visibility = "hidden";
        $.ajax("/booksapi")
        .done(function(data) {
            data = data['books'];
            for (var i = 0; i < data.length; i++ ) {
                var row = '<tr>';
                
                row +=  '<th scope="row">'+ (i+1) +'</th>'                
                row += '<td scope="col">' + '<img src="'+ data[i].smallThumbnail +'" class="rounded mx-auto d-block" alt="'+data[i].previewLink+'">' + '</th>';
                row += '<td scope="col">' + data[i].title + '</td>';                  
                row += '<td scope="col">' + data[i].authors + '</td>';
                row += '<td scope="col">' + data[i].publishedDate + '</td>';
                row += '<td scope="col"><div class="clickFav"><span class="fa fa-star-o"></span><div class="ring"></div><div class="ring2"></div><p class="infoFav">Added to favourites!</p></div></td>';
                row += '</tr>';                                   
                $('#volumes-table-body').append(row);
            }
        });
    });

    udpateFav();
    function udpateFav() {
        var numItems = $('.fa-star').length
        $('#favCount').text(numItems);
    }

    $('.clickFav').click(function() {
        if ($('span').hasClass("fa-star")) {
                $('.clickFav').removeClass('active')
            setTimeout(function() {
                $('.clickFav').removeClass('active-2')
            }, 30)
                $('.clickFav').removeClass('active-3')
            setTimeout(function() {
                $('span').removeClass('fa-star')
                $('span').addClass('fa-star-o')
            }, 15)
            udpateFav();
        } else {
            $('.clickFav').addClass('active')
            $('.clickFav').addClass('active-2')
            setTimeout(function() {
                $('span').addClass('fa-star')
                $('span').removeClass('fa-star-o')
            }, 150)
            setTimeout(function() {
                $('.clickFav').addClass('active-3')
            }, 150)
            $('.infoFav').addClass('infoFav-tog')
            setTimeout(function(){
                $('.infoFav').removeClass('infoFav-tog')
            },1000)
            udpateFav();
        }
    })

});


//---

"use strict";"object"!=typeof window.CP&&(window.CP={}),window.CP.PenTimer={programNoLongerBeingMonitored:!1,timeOfFirstCallToShouldStopLoop:0,_loopExits:{},_loopTimers:{},START_MONITORING_AFTER:2e3,STOP_ALL_MONITORING_TIMEOUT:5e3,MAX_TIME_IN_LOOP_WO_EXIT:2200,exitedLoop:function(o){this._loopExits[o]=!0},shouldStopLoop:function(o){if(this.programKilledSoStopMonitoring)return!0;if(this.programNoLongerBeingMonitored)return!1;if(this._loopExits[o])return!1;var t=this._getTime();if(0===this.timeOfFirstCallToShouldStopLoop)return this.timeOfFirstCallToShouldStopLoop=t,!1;var i=t-this.timeOfFirstCallToShouldStopLoop;if(i<this.START_MONITORING_AFTER)return!1;if(i>this.STOP_ALL_MONITORING_TIMEOUT)return this.programNoLongerBeingMonitored=!0,!1;try{this._checkOnInfiniteLoop(o,t)}catch(o){return this._sendErrorMessageToEditor(),this.programKilledSoStopMonitoring=!0,!0}return!1},_sendErrorMessageToEditor:function(){try{if(this._shouldPostMessage()){var o={action:"infinite-loop",line:this._findAroundLineNumber()};parent.postMessage(o,"*")}else this._throwAnErrorToStopPen()}catch(o){this._throwAnErrorToStopPen()}},_shouldPostMessage:function(){return document.location.href.match(/boomerang/)},_throwAnErrorToStopPen:function(){throw"We found an infinite loop in your Pen. We've stopped the Pen from running. Please correct it or contact support@codepen.io."},_findAroundLineNumber:function(){var o=new Error,t=0;if(o.stack){var i=o.stack.match(/boomerang\S+:(\d+):\d+/);i&&(t=i[1])}return t},_checkOnInfiniteLoop:function(o,t){if(!this._loopTimers[o])return this._loopTimers[o]=t,!1;var i=t-this._loopTimers[o];if(i>this.MAX_TIME_IN_LOOP_WO_EXIT)throw"Infinite Loop found on loop: "+o},_getTime:function(){return+new Date}},window.CP.shouldStopExecution=function(o){var t=window.CP.PenTimer.shouldStopLoop(o);return t===!0&&console.warn("[CodePen]: An infinite loop (or a loop taking too long) was detected, so we stopped its execution. Sorry!"),t},window.CP.exitedLoop=function(o){window.CP.PenTimer.exitedLoop(o)};




